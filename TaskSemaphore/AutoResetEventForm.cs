using System.Threading;

namespace TaskSemaphore
{

    public partial class AutoResetEventForm : Form
    {
        //写线程将数据写入myData
        int myData = 0;
        //读写次数
        int readWriteCount = 10000;

        #region 1、AutoResetEvent使用
        /*
     * AutoResetEvent对象有终止和非终止两种状态，终止状态是线程继续执行，非终止状态使线程阻塞，可以调用set和reset方法使对象进入终止和非终止状态
     * false：无信号，子线程的WaitOne方法不会被自动调用；
     * true：有信号，子线程的WaitOne方法会被自动调用。
     * AutoResetEvent(bool initialState)：构造函数，用一个指示是否将初始状态设置为终止的布尔值初始化该类的新实例。 false：无信号，子线程的WaitOne方法不会被自动调用 true：有信号，子线程的WaitOne方法会被自动调用
     * Reset ()：将事件状态设置为非终止状态，导致线程阻止；如果该操作成功，则返回true；否则，返回false。
     * Set ()：将事件状态设置为终止状态，允许一个或多个等待线程继续；如果该操作成功，则返回true；否则，返回false。
     * WaitOne()： 阻止当前线程，直到收到信号。
     * WaitOne(TimeSpan, Boolean) ：阻止当前线程，直到当前实例收到信号，使用 TimeSpan 度量时间间隔并指定是否在等待之前退出同步域。
     */
        //false:初始时没有信号
        AutoResetEvent auto = new AutoResetEvent(false);
        #endregion

        public AutoResetEventForm()
        {
            InitializeComponent();
            Load += Form1_Load;
        }

        private void Form1_Load(object? sender, EventArgs e)
        {
            #region 1、主线程写，子线程读，且只有将数据写入后，读线程才能将其读出
            //子线程读取
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    //在数据被写入前，读线程等待（实际上是等待写线程发出数据写完的信号）
                    auto.WaitOne();
                    this.Invoke(new Action(() =>
                    {
                        this.label1.Text = this.myData.ToString();
                    }));
                }
            });
            #endregion
        }

        private void button1_Click(object sender, EventArgs e)
        {
            #region 1、主线程写，子线程读，且只有将数据写入后，读线程才能将其读出
            Task.Factory.StartNew(() =>
            {
                //主线程写入
                for (int i = 0; i <= readWriteCount; i++)
                {
                    this.myData = i;
                    //写线程发信号，说明值已写过了,通知正在等待的线程有事件发生
                    auto.Set();
                    Thread.Sleep(100);
                }
            });
            #endregion
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //AutoResetEvent.WaitOne() 执行完毕 自动进入 reset() 状态
            auto.Reset();
        }
    }
}