using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using System.Xml.Linq;

namespace TaskSemaphore
{

    public partial class CountdownEventForm : Form
    {
        /**
         * CountdownEvent 实例化是需要传入一个int 类型作为InitialCount初始值，CountdownEvent信号量的释放很特别，只有当Countdown类的实例的CurrentCount等于0时才会释放我们的信号量，Signal()方法每次调用都会使得CurrentCount进行-1操作。Reset（）方法会重置为实例化对象时传递的参数值，也可以Reset(100)对我们的InitialCount重新赋值。
         * 
         */
        public CountdownEventForm()
        {
            InitializeComponent();
            Load += Form1_Load;
        }

        void CountDownTest()
        {
            ConcurrentQueue<int> queue = new ConcurrentQueue<int>(Enumerable.Range(0, 10));
            //初始化个数，当这个个数为0时，执行Wait()后的函数
            CountdownEvent cde = new CountdownEvent(10);

            //模拟消费逻辑
            Action consumer = () =>
            {
                int local;
                while (queue.TryDequeue(out local))
                {
                    Debug.WriteLine("当前消费者的ID:" + Task.CurrentId.Value);
                    cde.Signal();
                }
            };


            //模拟两个异步消费者
            Task t1 = Task.Factory.StartNew(consumer);
            Task t2 = Task.Factory.StartNew(consumer);

            //等待cde的数量达到0，才进行下面逻辑
            cde.Wait();

            Debug.WriteLine("CountdownEvent初始数量：{0},当前CountdownEvent数量：{1},当前CountdownEvent数量是否为0：{2}", cde.InitialCount, cde.CurrentCount, cde.IsSet);
            cde.Dispose();
        }

        private void Form1_Load(object? sender, EventArgs e)
        {
            CountDownTest();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }
    }
}