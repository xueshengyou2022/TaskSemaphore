using System.Threading;
using System.Xml.Linq;

namespace TaskSemaphore
{

    public partial class ManualResetEventForm : Form
    {
        //写线程将数据写入myData
        int myData = 0;
        //读写次数
        int readWriteCount = 10000;

        #region 2、ManualResetEvent使用
        /**
         * 通知一个或多个正在等待的线程已发生事件，允许线程通过发信号互相通信，来控制线程是否可心访问资源
         */
        ManualResetEvent mre = new ManualResetEvent(false);
        #endregion


        public ManualResetEventForm()
        {
            InitializeComponent();
            Load += Form1_Load;
        }

        private void Form1_Load(object? sender, EventArgs e)
        {
            //子线程读取
            Task.Factory.StartNew(() =>
            {
                while (myData <= readWriteCount)
                {
                    //在数据被写入前，读线程等待（实际上是等待写线程发出数据写完的信号）
                    mre.WaitOne();
                    this.Invoke(new Action(() =>
                    {
                        this.label1.Text = this.myData.ToString();
                    }));
                    myData += 1;
                    Thread.Sleep(10);
                }
            });
        }

        private void button1_Click(object sender, EventArgs e)
        {
            mre.Set();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            mre.Reset();
        }
    }
}